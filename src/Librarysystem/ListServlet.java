package Librarysystem;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ListServlet", urlPatterns = {"/Servlet"})
public class ListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        ((PrintWriter) out).println("<html><head></head><body>");
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String release_date = request.getParameter( "release_date");
        String username = request.getParameter( "username");
        String password = request.getParameter( "password");
        out.println("<h1>Super Duper Book App</h1>");
        out.println("<p> Title: " + title);
        out.println("<p> Author: " + author);
        out.println("<p> Release_Date"   + release_date);
        out.println("<h1> SuperSecret login information</h1>");
        out.println("<p> UserName: " + username);
        out.println("<p> Password: " + password);


        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out =response.getWriter();
        out.println("This resource is not available directly.");
    }
}
